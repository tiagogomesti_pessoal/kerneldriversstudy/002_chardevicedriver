obj-m := charDev.o

KERNELDIR ?= /home/tiagog/Projects/kernelStudy/kernelBase/linux-4.9.87/


all default: modules
install: modules_install

modules modules_install help clean:
	$(MAKE) -C $(KERNELDIR) M=$(shell pwd) ARCH=arm CROSS_COMPILE=/home/tiagog/Projects/kernelStudy/Compilers/gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf- $@ 

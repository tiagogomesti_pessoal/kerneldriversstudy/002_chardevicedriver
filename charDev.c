
// #include <linux/init.h>
// #include <linux/kernel.h>
// #include <linux/device.h>
// #include <linux/fs.h>
// #include <linux/version.h>
#include "linux/module.h"
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/interrupt.h>

#include <linux/timer.h>
#include <linux/jiffies.h>

#include <linux/gpio.h>

#include "lcdMatrix.h"



/**************************************************************/
/*                    Variaveis utilizadas                    */
/**************************************************************/
static unsigned int major; /* major number for device */
static struct class *dummy_class;
static struct cdev dummy_cdev;
static struct timer_list timer;
static char c = INITIAL_CHAR_VALUE;
static char reativeTimer = 0;
static int buttonGpio_irq;
static int buttonCount = 0;


#define DEV_NAME    "TG_dummyCharDev"
#define CLASS_NAME  "TG_dummyCharClass"

/**************************************************************/
/*                     Escopo das funcoes                     */
/**************************************************************/
static int __init dummy_char_init_module(void);
static void __exit dummy_char_cleanup_module(void);

int dummy_open(struct inode * inode, struct file * filp);
int dummy_release(struct inode * inode, struct file * filp);
ssize_t dummy_read (struct file *filp, char __user * buf, size_t count, loff_t * offset);
ssize_t dummy_write(struct file * filp, const char __user * buf, size_t count, loff_t * offset);

void timerScheduleExecution(unsigned long data);

void initLedMatrix(void);
void releaseLedMatrix(void);
void pinState(int pin, int state);
void spiTransfer(int addr, int opcode, int data);
void writeCharLedMatrix(char data);
void clearDisplay(void);

int requestGpios(void);
void releaseGpios(void);

static irqreturn_t r_irq_handler(int irq, void *dev_id);
static irqreturn_t r_irq_thread(int irq, void *dev_id);

void external_char(char c);



/**************************************************************/
/*               Informations aboute the module               */
/**************************************************************/
module_init(dummy_char_init_module);
module_exit(dummy_char_cleanup_module);

MODULE_AUTHOR("Tiago Gomes <tiagogomes.ti@gmail.com>");
MODULE_DESCRIPTION("Dummy character driver");
MODULE_LICENSE("GPL");


/**************************************************************/
/*            Estrutura para operacao com arquivos            */
/**************************************************************/
struct file_operations dummy_fops = {
  open:       dummy_open,
  release:    dummy_release,
  read:       dummy_read,
  write:      dummy_write,
};

/**************************************************************/
/*                Funcao de insercao do modulo                */
/**************************************************************/
static int __init dummy_char_init_module(void){
  struct device *dummy_device;
  int error;
  dev_t devt = 0;

  // request the gpios
  if(-1 == requestGpios()){
    pr_info("Gpio request Failed");
    pr_info("Module not loaded");
    return -1;
  }

  // Initialize the led matrix
  initLedMatrix();

  // Schedule execution Timer
  setup_timer(&timer, timerScheduleExecution, 0);
  // if( 0 != mod_timer(&timer, jiffies + msecs_to_jiffies(TIME_BETWEEN_EXEC)) ){
  //   pr_info("Timer firing failed\n");
  // }

  // Configure the button interruption
  // request_irq()
  if(0 > (buttonGpio_irq = gpio_to_irq(BUTTON_PIN))){
    pr_info("Button irq Failed");
    pr_info("Module not loaded");
    return -1;
  }

  else if(0 != request_threaded_irq(buttonGpio_irq               , 
                                    (irq_handler_t) r_irq_handler,
                                    (irq_handler_t) r_irq_thread ,
                                    IRQF_TRIGGER_FALLING         , 
                                    BUTTON_INT_NAME              ,
                                    NULL)
  ){
    pr_info("Button irq Failed");
    pr_info("Module not loaded");
    return -1;
  }


  // Get a range of minor numbers (starting with 0) to work with
  error = alloc_chrdev_region( &devt,        /* dev        - output parameter for first assigned number     */
                               0,            /* baseminor  - first of the requested range of minor numbers  */
                               1,            /* count      - the number of minor numbers required           */
                               DEV_NAME  /* name       - the name of the associated device or driver    */
                             );
  if ((error) < 0) {
    pr_err("Can't get major number\n");
    return error;
  }

  /*          Print information about the major number          */
  major = MAJOR(devt);
  pr_info("dummy_char major number = %d\n",major);
  
  /*         Create device class, visible in /sys/class         */  
  dummy_class = class_create(THIS_MODULE, CLASS_NAME);
  
  if (IS_ERR(dummy_class)) {
    pr_err("Error creating dummy char class.\n");
    unregister_chrdev_region(MKDEV(major, 0), 1);
    return PTR_ERR(dummy_class);
  }

  /* Initialize the char device and tie a file_operations to it */
  cdev_init(&dummy_cdev, &dummy_fops);
  dummy_cdev.owner = THIS_MODULE;
  
  /*      Now make the device live for the users to access      */
  cdev_add(&dummy_cdev, devt, 1);

  dummy_device = device_create( dummy_class ,
                                NULL        ,   /* no parent device */
                                devt        ,   /* associated dev_t */
                                NULL        ,   /* no additional data */
                                DEV_NAME        /* device name */
                              );  

  if (IS_ERR(dummy_device)) {
    pr_err("Error creating dummy char device.\n");
    class_destroy(dummy_class);
    unregister_chrdev_region(devt, 1);
    return -1;
  }

  /*                     Module initialized                     */
  pr_info("dummy char module loaded\n");
  return 0;
}

/*************************************************************/
/*                Funcao de remocao do modulo                */
/*************************************************************/
static void __exit dummy_char_cleanup_module(void){
  
  free_irq(gpio_to_irq(BUTTON_PIN), NULL);
  releaseLedMatrix();
  releaseGpios();
  del_timer(&timer);
  unregister_chrdev_region(MKDEV(major, 0), 1);
  device_destroy(dummy_class, MKDEV(major, 0));
  cdev_del(&dummy_cdev);
  class_destroy(dummy_class);

  pr_info("dummy char module Unloaded\n");
}

/************************************************************/
/*      Funcoes de operacoes de arquivo do char device      */
/************************************************************/

/*----------------------------------------------------------*/
/*-                          Open                          -*/
/*----------------------------------------------------------*/
int dummy_open(struct inode * inode, struct file * filp){

  if(timer_pending(&timer)){
    del_timer(&timer);
    reativeTimer = 1;
  }
  else{
    reativeTimer = 0;
  }

  return 0;
}

/*-----------------------------------------------------------*/
/*-                          Close                          -*/
/*-----------------------------------------------------------*/
int dummy_release(struct inode * inode, struct file * filp){
  
  if(reativeTimer){
    setup_timer(&timer, timerScheduleExecution, 0);
    
    if( 0 != mod_timer(&timer, jiffies + msecs_to_jiffies(TIME_BETWEEN_EXEC)) ){
      pr_info("Timer firing failed\n");
    }
  }


  pr_info("Someone closed me\n");

  return 0;
}

/*----------------------------------------------------------*/
/*-                          read                          -*/
/*----------------------------------------------------------*/
ssize_t dummy_read (struct file *filp, char __user * buf, size_t count, loff_t * offset){
  pr_info("Nothing to read guy\n");
  return 0;
}

/*-----------------------------------------------------------*/
/*-                          write                          -*/
/*-----------------------------------------------------------*/
ssize_t dummy_write(struct file * filp, const char __user * buf, size_t count, loff_t * offset){
  char *data;
  int i;

  if(NULL == (data = kzalloc((count+1)*sizeof(char), GFP_KERNEL))){
    pr_info("Data not allocated");  
    return 0;
  }

  if (0 != copy_from_user(data, buf, count)){
    pr_info("Data not copied");  
    return 0;
  }
  *(data+count) = 0;  // string end

  pr_info("count = %d - data = %s", count, data);

  for(i=0; i<count; i++){
    if( *(data+i) == 0x02){ // clear display
      clearDisplay();
      pr_info("clear display command");
      return count;
    }
    else if( *(data+i) == 0x03){ // test mode
      pr_info("Display test initialized");
      spiTransfer(0, OP_DISPLAYTEST, 1);
      msleep(DISPLAY_TEST_TIME);
      spiTransfer(0, OP_DISPLAYTEST, 0);
      return count;
    }

    if( ( *(data+i) < 0x20) || ( *(data+i) > 'z') ){
      pr_info("Char not supported");
      return 0;  
    }

    writeCharLedMatrix(*(data+i));
    msleep(TIME_BETWEEN_CHAR);
  }


  return count;
}

/************************************************************/
/*  Funcoes relativas a temporizacao de execucao do modulo  */
/************************************************************/
void timerScheduleExecution(unsigned long data){
  int i;
  char stringTest[50];

  if((c < INITIAL_CHAR_VALUE) || (c > FINAL_CHAR_VALUE) ){
    c = INITIAL_CHAR_VALUE;
  }
  
  writeCharLedMatrix(c++);
  mod_timer(&timer, jiffies + msecs_to_jiffies(TIME_BETWEEN_EXEC));
}

/************************************************************/
/*            Funcoes relativas a matrix de leds            */
/************************************************************/
void initLedMatrix(void){
    // spiTransfer(0, OP_SHUTDOWN, SHUTDOWN_MODE);
  spiTransfer(0, OP_SHUTDOWN, NORMAL_OPERARION);
  spiTransfer(0, OP_INTENSITY, 0);
  spiTransfer(0, OP_DISPLAYTEST, 0);
  spiTransfer(0, OP_SCANLIMIT, 7);
  clearDisplay();
}

void releaseLedMatrix(void){
  clearDisplay();
  spiTransfer(0, OP_SHUTDOWN, SHUTDOWN_MODE);
}

void pinState(int pin, int state){
  gpio_set_value(pin, state);
}

void spiTransfer(int addr, int opcode, int data){
  int spiData = 0x00;
  int i, shiftAux;

  spiData = ((opcode<<8) & 0x0F00) | (data & 0x00FF);

  gpio_set_value(LD_PIN , LOW);
  gpio_set_value(CLK_PIN, LOW);

  // shiftAux = 1; 
  shiftAux = 0x8000; 

  for(i=0; i<SPI_WORD_LEN; i++){
    if(0 != (shiftAux & spiData)){ // set
      pinState(MOSI_PIN, HIGH);
      // pr_info("1");
    }
    else{ // clear
      pinState(MOSI_PIN, LOW);
      // pr_info("0");
    }
    // udelay(1);

    // shiftAux = shiftAux<<1;
    shiftAux = shiftAux>>1;

    pinState(CLK_PIN, HIGH);
    udelay(SPI_CLK_PERIOD_HALF);
    pinState(CLK_PIN, LOW);
    udelay(SPI_CLK_PERIOD_HALF);
  }

  gpio_set_value(LD_PIN , HIGH);
  // msleep(5);
  // gpio_set_value(LD_PIN , LOW);
}

void writeCharLedMatrix(char data){
  int i;
  int charToLcdMatrix;
  unsigned char *lcdChar;
    
  lcdChar = &font5x7[data-0x20][0];

  for(i=ROW_0; i<=ROW_7; i++){
    charToLcdMatrix   = *(lcdChar+i-1);
    spiTransfer(0, i, charToLcdMatrix);
  }
}

void clearDisplay(void){
  int i;

  pr_info("reseting the display");
  for(i=ROW_0; i<=1*ROW_7; i++){
    // spiTransfer(0, i, 0);
    spiTransfer(0, i, 0);
  }
}


/*************************************************************/
/*                 Funcoes relativas ao GPIO                 */
/*************************************************************/
int requestGpios(void){
  char clkPin_request;
  char mosiPin_request;
  char ldPin_request;
  char buttonGpio_request;

  clkPin_request     = gpio_request(CLK_PIN    , "clkPin" );
  mosiPin_request    = gpio_request(MOSI_PIN   , "mosiPin");
  ldPin_request      = gpio_request(LD_PIN     , "ldPin"  );
  buttonGpio_request = gpio_request(BUTTON_PIN , "buttonGpio_irq");

  pr_info("clkPin_request = %d\n"
          "mosiPin_request = %d\n"
          "ldPin_request = %d\n"
          "buttonGpio_request = %d\n",
          clkPin_request,
          mosiPin_request,
          ldPin_request,
          buttonGpio_request
         );


  if(0 != clkPin_request  ||
     0 != mosiPin_request ||
     0 != ldPin_request   ||
     0 != buttonGpio_request
  ){    
    if(0 == clkPin_request){
      gpio_free(CLK_PIN);
    }
    if(0 == mosiPin_request){
      gpio_free(MOSI_PIN);
    }
    if(0 == ldPin_request){
      gpio_free(LD_PIN);
    }
    if(0 == buttonGpio_request){
      gpio_free(BUTTON_PIN);
    }

    pr_info("Error to allocate pins\n");
    return -1;
  }
  else{
    gpio_direction_output(CLK_PIN   , LOW);
    gpio_direction_output(MOSI_PIN  , LOW);
    gpio_direction_output(LD_PIN    , HIGH);
    
    gpio_direction_input (BUTTON_PIN);
    gpio_set_debounce    (BUTTON_PIN, BUTTON_DEBOUNCE);
    gpio_export(BUTTON_PIN, false);
    
    pr_info("OPEN - All pins allocated sucessfull\n");
    return 0;    
  }
}

void releaseGpios(void){
  gpio_free(CLK_PIN);
  gpio_free(MOSI_PIN);
  gpio_free(LD_PIN);  

  gpio_unexport(BUTTON_PIN);
  gpio_free(BUTTON_PIN);
}


/*************************************************************/
/*              Funcoes relativas a Interrupcap              */
/*************************************************************/
static irqreturn_t r_irq_handler(int irq, void *dev_id){
  int i;

  pr_info("irq %d - Button Pressed - %d", irq, buttonCount++);

  return IRQ_WAKE_THREAD;
}

static irqreturn_t r_irq_thread(int irq, void *dev_id){
  int i;
  char strTest[50];

  sprintf(strTest, STRING_IRQ_TEST);

  if(timer_pending(&timer)){
    del_timer(&timer);
    reativeTimer = 1;
  }
  else{
    reativeTimer = 0;
  }

  for(i=0; i<strlen(strTest); i++){
    writeCharLedMatrix(strTest[i]);
    msleep(TIME_BETWEEN_CHAR);
  }

  if(reativeTimer){
    setup_timer(&timer, timerScheduleExecution, 0);
    
    if( 0 != mod_timer(&timer, jiffies + msecs_to_jiffies(TIME_BETWEEN_EXEC)) ){
      pr_info("Timer firing failed\n");
    }
  }

  return IRQ_HANDLED;  
}

/*************************************************************/
/*              Funcoes relativas a Interrupcap              */
/*************************************************************/
void external_char(char c){
  pr_info("char receipted - %c", c);
  writeCharLedMatrix(c);
}

EXPORT_SYMBOL(external_char);



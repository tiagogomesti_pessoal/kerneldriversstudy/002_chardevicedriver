/*
 * main.c
 *
 *  Created on: Jan 16, 2019
 *      Author: tiagog
 */
#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include "stdint.h"
#include <unistd.h>
#include "string.h"
#include "unistd.h"

#define DEV_NAME  "/dev/TG_dummyCharDev"
#define NUM_LOOP	2

int main(int argc, char *argv[]){
	int16_t fd;
	uint8_t i, c;
	uint16_t len;
	uint8_t status = 0;

	if(0 >= (fd = open(DEV_NAME, O_RDWR | O_NONBLOCK))){
		perror("");
		status = -1;
	}
	else{
		len = strlen(argv[1]);
		if(len == 1){
			c = *argv[1];
				if(sizeof(c) != write(fd, &c, sizeof(c))){
					printf("write error!\n");
					status = -1;
				}
		}
		else if(len > 1){
			for(i=0; i<len; i++){
				c = *(argv[1] + i);
				if(sizeof(c) != write(fd, &c, sizeof(c))){
					printf("write error!\n");
					status = -1;
					break;
				}
				sleep(1);
			}
		}
		else{
			status = -1;
			printf("No string inserted...\n");
		}
	}

	close(fd);
	printf("device closed\n");

	return status;
}
